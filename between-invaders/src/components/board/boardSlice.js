import { createSlice } from '@reduxjs/toolkit';

export const boardSlice = createSlice({
    name: 'board',
    initialState: {
        gameOver: false,
        pause: false,
    },
    reducers: {
        switchPause: state => {
            state.pause = !state.pause;
        },
        lose: state => {
            state.gameOver = true
            state.pause = true;
        },
    },
})

export default boardSlice.reducer;

export const { switchPause, lose } = boardSlice.actions;

export const selectPause = state => state.board.pause;
export const selectGameOver = state => state.board.gameOver;
