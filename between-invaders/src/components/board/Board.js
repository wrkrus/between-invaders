import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { lose, selectGameOver, selectPause } from './boardSlice';
import { selectHit, selectInvadingCount } from '../enemies/enemiesSlice';
import { Player } from '../player/Player';
import { Bullets } from '../bullets/Bullets';
import { Enemies } from '../enemies/Enemies';
import { switchPause } from './boardSlice';
import { INVADING_MAX_COUNT, SPACE, RESTART_ACTION_TYPE } from '../../constants';

import './Board.css'

export function Board() {

    const hit = useSelector(selectHit);
    const isGameOver = useSelector(selectGameOver);
    useEffect(() => {
        if (hit) {
            dispatch(lose());
        }

        document.addEventListener('keydown', handleKeydown);
        return function cleanup() {
            document.removeEventListener('keydown', handleKeydown);
        };
    }, [hit, isGameOver]);

    const dispatch = useDispatch();
    const invadingCount = useSelector(selectInvadingCount);
    useEffect(() => {
        if (invadingCount >= INVADING_MAX_COUNT) {
            dispatch(lose());
        }
    }, [invadingCount]);

    const handleKeydown = (event) => {
        if (event.code === SPACE) {
            if (isGameOver) {
                dispatch({ type: RESTART_ACTION_TYPE });
            } else {
                dispatch(switchPause());
            }
        }
    }

    const pause = useSelector(selectPause);
    return (
        <div className="board">
            { pause && (
                <div className="overlay" />
            )}
            <Player />
            <Bullets />
            <Enemies />
        </div>
    );
}