import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { down, up, selectPosition } from './playerSlice';
import { shoot } from '../bullets/bulletsSlice';
import { selectPause } from '../board/boardSlice';
import { PLAYER_HEIGHT } from './constants';
import { ARROW_UP, ARROW_DOWN, ARROW_LEFT, ARROW_RIGHT, TO_LEFT, TO_RIGHT } from '../../constants';

import playerIcon from '../../static/player.png'
import styles from './styles';


export function Player() {

    const pause = useSelector(selectPause);
    const position = useSelector(selectPosition);
    useEffect(() => {
        if (!pause) {
            document.addEventListener('keydown', handleKeydown);
        }
        return function cleanup() {
            if (!pause) {
                document.removeEventListener('keydown', handleKeydown);
            }
        };
    }, [pause, position]);

    const dispatch = useDispatch();
    const handleKeydown = (event) => {
        if (event.code === ARROW_UP) {
            dispatch(up());
        }
        if (event.code === ARROW_DOWN) {
            dispatch(down());
        }
        if (event.code === ARROW_LEFT) {
            dispatch(shoot({ positionTop: position + PLAYER_HEIGHT / 2, direction: TO_LEFT }));
        }
        if (event.code === ARROW_RIGHT) {
            dispatch(shoot({ positionTop: position + PLAYER_HEIGHT / 2, direction: TO_RIGHT }));
        }
    }

    return (
        <div
            className="player"
            style={ { ...styles.player, top: `${ position }%` } }
        >
            <img src={ playerIcon } style={ { ...styles.playerIcon } } />
        </div>
    )
}
