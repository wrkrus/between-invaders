import { createSlice } from '@reduxjs/toolkit';

import { PLAYER_HEIGHT } from './constants';

export const playerSlice = createSlice({
    name: 'player',
    initialState: {
        position: 50 - PLAYER_HEIGHT / 2,
    },
    reducers: {
        up: state => {
            if ( state.position > 0) {
                state.position -= 3
            }
        },
        down: state => {
            if ( state.position < 90) {
                state.position += 3
            }
        },
    },
})

export default playerSlice.reducer;

export const { up, down } = playerSlice.actions;

export const selectPosition = state => state.player.position;
