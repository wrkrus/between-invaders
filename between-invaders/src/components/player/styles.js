import { PLAYER_WIDTH, PLAYER_HEIGHT } from './constants';

export default {
    'player': {
        position: 'absolute',
        left: `calc(50% - ${ PLAYER_WIDTH/2 }%)`,
        width: `${ PLAYER_WIDTH }%`,
        height: `${ PLAYER_HEIGHT }%`,
    },
    'playerIcon': {
        width: '100%',
        height: '100%',
        background: 'transparent',
    }
}
