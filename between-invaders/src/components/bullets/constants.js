export const BULLET_WIDTH = 0.5;
export const BULLET_HEIGHT = 0.5;

export const BULLET_MOVEMENT_INTERVAL = 50;
