import PropTypes from 'prop-types';

import styles from './styles';

export function Bullet(props) {
    return (
        <div
            className="bullet"
            style={ {
                ...styles.bullet,
                top: `${ props.positionTop }%`,
                left: `${ props.positionLeft }%`
            } }
        />
    );
}

Bullet.propTypes = {
    positionTop: PropTypes.number.isRequired,
    positionLeft: PropTypes.number.isRequired,
}
