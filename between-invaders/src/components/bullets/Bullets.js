import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Bullet } from './Bullet';
import { move, hit, destroy, selectBullets, selectHitBullets } from './bulletsSlice';
import { selectEnemies } from '../enemies/enemiesSlice';
import { selectPause } from '../board/boardSlice';
import { BULLET_MOVEMENT_INTERVAL } from './constants';

export function Bullets() {

    const pause = useSelector(selectPause);
    useEffect(() => {
        let interval;
        if (!pause) {
            interval = setInterval(moveBullets, BULLET_MOVEMENT_INTERVAL);
        }
        return () => {
            if (interval) {
                clearInterval(interval);
            }
        }
    }, [pause]);

    const dispatch = useDispatch();
    const bullets = useSelector(selectBullets);
    const moveBullets = () => dispatch(move());

    const enemies = useSelector(selectEnemies);
    useEffect(() => {
        dispatch(hit({ enemies: enemies }));
    }, [bullets]);

    const hitBullets = useSelector(selectHitBullets);
    useEffect(() => {
        if (hitBullets.length > 0) {
            dispatch(destroy({ bulletKeys: hitBullets }));
        }
    }, [hitBullets])

    return (
        <>
            {
                bullets.map(bullet => (
                    <Bullet { ...bullet } />
                ))
            }
        </>
    );
}
