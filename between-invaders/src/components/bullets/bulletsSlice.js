import { createSlice } from '@reduxjs/toolkit';

import { isBulletHitEnemy } from '../../helpers';

export const bulletsSlice = createSlice({
    name: 'bullets',
    initialState: {
        bullets: [],
        hitBullets: [],
        hitEnemies: [],
        hitBulletCount: 0,
    },
    reducers: {
        shoot: (state, action) => {
            state.bullets.push({
                ...action.payload,
                positionLeft: 50 + action.payload.direction * 2,
                key: Date.now(),
            });
        },
        move: state => {
            const bullets = [];
            for (const bullet of state.bullets) {
                const positionLeft = bullet.positionLeft + bullet.direction;
                if (0 < positionLeft && positionLeft < 100) {
                    bullets.push({ ...bullet, positionLeft: positionLeft});
                }
            }
            state.bullets = bullets;
        },
        hit: (state, action) => {
            const hitBullets = [];
            const hitEnemies = [];
            for (const bullet of state.bullets) {
                for (const enemy of action.payload.enemies) {
                    if (isBulletHitEnemy(bullet, enemy)) {
                        hitBullets.push(bullet.key);
                        hitEnemies.push(enemy.key)
                    }
                }
            }

            state.hitBullets = hitBullets;
            state.hitEnemies = hitEnemies;
            state.hitBulletCount += hitBullets.length;
        },
        destroy: (state, action) => {
            state.bullets = state.bullets.filter(bullet => !action.payload.bulletKeys.includes(bullet.key));
        }
    },
})

export default bulletsSlice.reducer;

export const { shoot, move, hit, destroy } = bulletsSlice.actions;

export const selectBullets = state => state.bullets.bullets;
export const selectHitBulletCount = state => state.bullets.hitBulletCount;
export const selectHitBullets = state => state.bullets.hitBullets;
export const selectHitEnemies = state => state.bullets.hitEnemies;
