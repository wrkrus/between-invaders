import { BULLET_WIDTH, BULLET_HEIGHT } from './constants';

export default {
    'bullet': {
        position: 'absolute',
        width: `${ BULLET_WIDTH }%`,
        height: `${ BULLET_HEIGHT }%`,
        borderRadius: '50%',
        backgroundColor: 'deepskyblue',
    }
}
