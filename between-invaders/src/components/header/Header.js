import './Header.css'

import { useSelector } from 'react-redux';
import { selectHitBulletCount } from '../bullets/bulletsSlice';
import { selectInvadingCount } from '../enemies/enemiesSlice';
import { INVADING_MAX_COUNT } from '../../constants';
import { selectGameOver } from '../board/boardSlice';

export function Header() {
    const score = useSelector(selectHitBulletCount);
    const invasion = useSelector(selectInvadingCount);
    const isGameOver = useSelector(selectGameOver);
    return (
        <div className="header">
            <div className="score">Score: { score }</div>
            { isGameOver && <div className="game-over">Game over</div>}
            <div className="invasion">Invasion: { invasion }/{ INVADING_MAX_COUNT }</div>
        </div>
    );
}