import './Footer.css'

export function Footer() {
    return (
        <div className="footer">
            <div>↑ ↓ - move</div>
            <div>space - pause/restart</div>
            <div>← → - shoot</div>
        </div>
    );
}
