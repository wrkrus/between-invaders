import { createSlice } from '@reduxjs/toolkit';

import { ENEMY_WIDTH, ENEMY_HEIGHT } from './constants';
import { DIRECTIONS, TO_RIGHT } from '../../constants';
import { isEnemyHitPlayer } from '../../helpers';


export const enemiesSlice = createSlice({
    name: 'enemies',
    initialState: {
        enemies: [],
        hit: false,
        invadingCount: 0,
    },
    reducers: {
        create: state => {
            const direction = DIRECTIONS[Math.floor(Math.random()*DIRECTIONS.length)];
            state.enemies.push({
                positionTop: Math.floor(Math.random()*19) * 5 + ENEMY_HEIGHT / 2,
                positionLeft: direction === TO_RIGHT ? -ENEMY_WIDTH : 100,
                direction: direction,
                key: Date.now(),
            });
        },
        move: state => {
            const enemies = [];
            let invadingCount = 0;
            for (const enemy of state.enemies) {
                const positionLeft = enemy.positionLeft + enemy.direction;
                if (0 - ENEMY_WIDTH < positionLeft && positionLeft < 100 + ENEMY_WIDTH) {
                    enemies.push({ ...enemy, positionLeft: positionLeft});
                } else {
                    invadingCount++;
                }
            }
            state.enemies = enemies;
            state.invadingCount += invadingCount;
        },
        hit: (state, action) => {
            for (const enemy of state.enemies) {
                if (isEnemyHitPlayer(enemy, action.payload.playerPosition)) {
                    state.hit = true;
                }
            }
        },
        destroy: (state, action) => {
            state.enemies = state.enemies.filter(enemy => !action.payload.enemyKeys.includes(enemy.key));
        }
    },
})

export default enemiesSlice.reducer;

export const { create, move, hit, destroy } = enemiesSlice.actions;

export const selectEnemies = state => state.enemies.enemies;
export const selectHit = state => state.enemies.hit;
export const selectInvadingCount = state => state.enemies.invadingCount;
