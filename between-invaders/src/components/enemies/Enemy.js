import PropTypes from 'prop-types';

import enemyIcon from '../../static/enemy.png'
import styles from './styles';
import './Enemies.css';

export function Enemy(props) {
    return (
        <div
            className="enemy"
            style={ {
                ...styles.enemy,
                top: `${ props.positionTop }%`,
                left: `${ props.positionLeft }%`
            } }
        >
            <img src={ enemyIcon } style={ { ...styles.enemyIcon } } />
        </div>
    );
}

Enemy.propTypes = {
    positionTop: PropTypes.number.isRequired,
    positionLeft: PropTypes.number.isRequired,
}
