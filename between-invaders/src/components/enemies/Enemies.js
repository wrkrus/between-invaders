import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Enemy } from './Enemy';
import { create, move, hit, destroy, selectEnemies } from './enemiesSlice';
import { selectHitEnemies } from '../bullets/bulletsSlice';
import { selectPause } from '../board/boardSlice';
import { ENEMY_CREATION_INTERVAL, ENEMY_MOVEMENT_INTERVAL } from './constants';
import { selectPosition } from '../player/playerSlice';

export function Enemies() {

    const pause = useSelector(selectPause);
    useEffect(() => {
        let createInterval, moveInterval;
        if (!pause) {
            dispatch(create())
            createInterval = setInterval(createEnemy, ENEMY_CREATION_INTERVAL)
            moveInterval = setInterval(moveEnemies, ENEMY_MOVEMENT_INTERVAL);
        }
        return () => {
            if (createInterval) {
                clearInterval(createInterval);
                clearInterval(moveInterval);
            }
        }
    }, [pause]);

    const enemies = useSelector(selectEnemies);
    const position = useSelector(selectPosition);
    useEffect(() => {
        dispatch(hit({ playerPosition: position }));
    }, [enemies, [position]]);

    const hitEnemies = useSelector(selectHitEnemies);
    useEffect(() => {
        if (hitEnemies.length > 0) {
            dispatch(destroy({ enemyKeys: hitEnemies }));
        }
    }, [hitEnemies])

    const dispatch = useDispatch();
    const createEnemy = () => dispatch(create());
    const moveEnemies = () => dispatch(move());

    return (
        <>
            {
                enemies.map(enemy => (
                    <Enemy { ...enemy } />
                ))
            }
        </>
    );
}
