import { ENEMY_WIDTH, ENEMY_HEIGHT } from './constants';

export default {
    'enemy': {
        position: 'absolute',
        padding: '3px',
        width: `${ ENEMY_WIDTH }%`,
        height: `${ ENEMY_HEIGHT }%`,
        borderRadius: '10%',
    },
    'enemyIcon': {
        width: '100%',
        height: '100%',
        background: 'transparent',
    }
}
