export const ENEMY_WIDTH = 5;
export const ENEMY_HEIGHT = 6;

export const ENEMY_CREATION_INTERVAL = 2000;
export const ENEMY_MOVEMENT_INTERVAL = 100;
