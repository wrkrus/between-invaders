import { BULLET_WIDTH, BULLET_HEIGHT } from './components/bullets/constants';
import { ENEMY_WIDTH, ENEMY_HEIGHT } from './components/enemies/constants';
import { PLAYER_WIDTH, PLAYER_HEIGHT } from './components/player/constants';


export function isBulletHitEnemy(bullet, enemy) {
    const bulletTop = bullet.positionTop;
    const bulletBottom = bullet.positionTop + BULLET_HEIGHT;
    const bulletLeft = bullet.positionLeft;
    const bulletRight = bullet.positionLeft + BULLET_WIDTH;
    

    const enemyTop = enemy.positionTop;
    const enemyBottom = enemy.positionTop + ENEMY_HEIGHT;
    const enemyLeft = enemy.positionLeft;
    const enemyRight = enemy.positionLeft + ENEMY_WIDTH;
    
    return (
        bulletTop <= enemyBottom && bulletBottom >= enemyTop && bulletLeft <= enemyRight && bulletRight >= enemyLeft
    );
}

export function isEnemyHitPlayer(enemy, playerPosition) {
    const enemyTop = enemy.positionTop;
    const enemyBottom = enemy.positionTop + ENEMY_HEIGHT;
    const enemyLeft = enemy.positionLeft;
    const enemyRight = enemy.positionLeft + ENEMY_WIDTH;
    
    const playerTop = playerPosition;
    const playerBottom = playerPosition + PLAYER_HEIGHT;
    const playerLeft = 50 - PLAYER_WIDTH / 2;
    const playerRight = 50 + PLAYER_WIDTH / 2;
    
    return (
        playerTop <= enemyBottom && playerBottom >= enemyTop && playerLeft <= enemyRight && playerRight >= enemyLeft
    );
}
