export const TO_RIGHT = 1;
export const TO_LEFT = -1;
export const DIRECTIONS = [TO_RIGHT, TO_LEFT]

export const INVADING_MAX_COUNT = 4;

export const ARROW_UP = 'ArrowUp';
export const ARROW_DOWN = 'ArrowDown';
export const ARROW_LEFT = 'ArrowLeft';
export const ARROW_RIGHT = 'ArrowRight';
export const SPACE = 'Space';

export const RESTART_ACTION_TYPE = 'all/restart'
