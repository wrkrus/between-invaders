import { Board } from './components/board/Board';
import { Header } from './components/header/Header';
import { Footer } from './components/footer/Footer';

function App() {
    return (
        <div className="app">
            <Header />
            <Board />
            <Footer />
        </div>
    );
}

export default App;
