import { combineReducers, configureStore } from '@reduxjs/toolkit';

import playerReducer from './components/player/playerSlice';
import bulletsReducer from './components/bullets/bulletsSlice';
import enemiesReducer from './components/enemies/enemiesSlice'
import boardReducer from './components/board/boardSlice'
import { RESTART_ACTION_TYPE } from './constants';

const combinedReducer = combineReducers({
  player: playerReducer,
  bullets: bulletsReducer,
  enemies: enemiesReducer,
  board: boardReducer,
});

const rootReducer = (state, action) => {
  if (action.type === RESTART_ACTION_TYPE) {
    state = undefined;  // refresh all reducers
  }
  return combinedReducer(state, action);
};

export default configureStore({
  reducer: rootReducer,
});
